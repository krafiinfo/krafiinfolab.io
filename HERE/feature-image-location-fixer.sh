#!/bin/bash

files=($(find . -name 'index.html'))

for file in "${files[@]}"
do
  echo "$file"

  cp "$file" ./docs/old.index.html
  sed 's/loading="lazy"/loading="eager"/g' "$file" > temp
  cp temp "$file"


  cp "$file" ./docs/old.index.html
  sed 's/.pngg/.png/' "$file" > temp
  cp temp "$file"

  sed 's/.pngng/.png/' "$file" > temp
  cp temp "$file"

  sed 's/.pngpng/.png/' "$file" > temp
  cp temp "$file"

  sed 's/.png.png/.png/' "$file" > temp
  cp temp "$file"

  sed 's/.png.pngg/.png/' "$file" > temp
  cp temp "$file"

  sed 's/.png.pngng/.png/' "$file" > temp
  cp temp "$file"

  sed 's/.png.pngpng/.png/' "$file" > temp
  cp temp "$file"

  sed 's/.png.png.png/.png/' "$file" > temp
  cp temp "$file"

  mv temp "$file"
done

echo "successfully completed. modifying all index.html files!" && sleep 3s

